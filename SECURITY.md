<!--
SPDX-FileCopyrightText: 2023 - 2024 Arm Limited and/or its affiliates <open-source-office@arm.com>

SPDX-License-Identifier: Apache-2.0
-->

## Reporting a Vulnerability

If you identify a vulnerability then please report the issue to the Arm Product Security Incident Response Team <psirt@arm.com>.

## Third Party Dependencies

Scripts within this project may download and patch third party sources.
These third party sources are:
* Google Test 1.12.1.
* Google Benchmark 1.8.3.
* OpenCV 4.11.0 (and its dependencies)
